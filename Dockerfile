FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/carproject.jar app.jar

ENTRYPOINT ["java","-jar","app.jar"]
EXPOSE 2222


